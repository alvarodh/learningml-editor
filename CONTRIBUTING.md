# Contributing to LearningML editor

LearningML is an open source project aiming to gather people, enterprises and
institutions interested in AI learning and teaching. You can contribute to the
project in several ways:

- as a developer,
- as a teacher,
- as a researcher

# Contributing as a developer

We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's by:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer
- Testing the tool
- Writing documentation

## We Develop with GitLab

We use GitLab to host code, to track issues and feature requests, as well as accept merge requests.

## How to submit fixes and new features 

We use the *Fork-and-Branch* git workflow, so all code changes happen through Merge Requests. If you are interested in fixing a bug or adding a new feature you should proceed as follow.

1. From the [list os issues](https://gitlab.com/learningml/learningml-editor/-/issues), select a feature to develop or a bug to fix. If
   there aren't any issues related to the work you are intended to do, you should create a new issue.

2. Use the comments of the issue to notify your intention of contributing to
   such issue. Here you should specify how you are going to solve the problem, 
   and, if the issue have been created by you, describe it in detail.

3. Create a fork of the project (you need a GitLab account).

4. Make a local clone of your forked project.

5. Checkout to `develop` branch and create a new branch from it. If you are 
   going to develop a new feature name it `feature/[feature-name]`. If your work
   is to fix a bug, name it `bugfix/[bug-name]`.

6. Work in your new branch as wanted and push the code as frequently as you 
   want. You can make new branches to experiment new ideas, or whatever you
   want, it's your fork, therefore you can work at your pace and style :-).

7. Once the feature/bugfix have been finished you must make a "merge request" of
   your branch to the `develop` branch of LearningML-editor repository. If you 
   have developed a new feature, you must include, at least, a documentation 
   where the feature is explained and with some examples to check how it works.
   It would very appreciate to have a battery of automatic unit test.

8. Your code will be reviewed and tested. During this phase a discussion will 
   take place between reviewers and developers (usually you as developer and
   a maintainer as reviewer). Finally, when everything is clear and working 
   right, your branch will be merged into the `develop` branch of the LearningML 
   editor repository. These changes will also be merged with `master` branch when a new release goes on-line.

9. Optionally, y you wish, you can update the master branch of your fork to be
   sync with the master branch of LearningML editor repo.

Scott Lowe gives a [great explanation](https://blog.scottlowe.org/2015/01/27/using-fork-branch-git-workflow/) about the *Fork-and-Branch* Git Workflow 
that can help you a lot.

## Any contributions you make will be under the AGPL Software License
In short, when you submit code changes, your submissions are understood to be under the same [GNU Affero GPL](https://www.gnu.org/licenses/agpl-3.0.en.html) 
that covers the project. Feel free to contact the maintainers if that is a concern.

## Report bugs using GitLab's [issues](https://gitlab.com/learningml/learningml-editor/-/issues)
We use GitLab issues to track public bugs. Report a bug by [opening a new issue](https://gitlab.com/learningml/learningml-editor/-/issues); it's that easy!

## Write bug reports with detail, background, and sample code
In order to be well understood by developers, bug reports must be written with
detail. The most important thing is to facilitate how to reproduce the bug and 
what is the expected behavior.

[This is an example](http://stackoverflow.com/q/12488905/180626) of a well 
written bug report. Here's another example (http://www.openradar.me/11905408).

**Great Bug Reports** tend to have:

- A quick summary and/or background
- Steps to reproduce
  - Be specific!
  - Give sample code if you can.
- What you expected would happen
- What actually happens
- Notes (possibly including why you think this might be happening, or stuff you
  tried that didn't work)

People *love* thorough bug reports. I'm not even kidding.

## Use a Consistent Coding Style
* 2 spaces for indentation rather than tabs
* 80 character line length
* You can try running `npm run lint` for style unification

## License
By contributing, you agree that your contributions will be licensed under its GNU Affero GPL License.


# Contributing as teacher

If you are a teacher, you can contribute by sharing AI activities in which 
LearningML is used as part of the activity or by proposing any ideas you 
have on how to use LearningML in the classroom. You can send your proposals
to email address `develop@learningml.es`.

# Contributing as a researcher

LearningML is also a Computing Education Research project. Therefore, if you
are a researcher willing to develop any experience on AI/ML learning
and teaching, we encourage you to use LearningML as a possible educative 
platform to carry out your research. So far, the following papers
have been written about LearningML:

- Rodríguez-García, J. D., Moreno-León, J., Román-González, M., & Robles,
  G. Evaluation of an Online Intervention to Teach Artificial Intelligence With
  LearningML to 10-16-Year-Old Students.
  https://www.researchgate.net/publication/344744220_Evaluation_of_an_Online_Intervention_to_Teach_Artificial_Intelligence_With_LearningML_to_10-16-Year-Old_Students

- Rodríguez-García, J. D., Moreno-León, J., Román-González, M., & Robles,
  G. (2020, October). Introducing Artificial Intelligence Fundamentals with 
  LearningML: Artificial Intelligence made easy. In Eighth International 
  Conference on Technological Ecosystems for Enhancing Multiculturality 
  (pp. 18-20).
  https://www.researchgate.net/publication/348697319_Introducing_Artificial_Intelligence_Fundamentals_with_LearningML_Artificial_Intelligence_made_easy

- Rodríguez-García, J. D., Moreno-León, J., Román-González, M., & Robles, G. (2020). 
  LearningML: A Tool to Foster Computational Thinking Skills through Practical 
  Artificial Intelligence Projects. Revista de Educación a Distancia (RED),
  20(63).
  https://revistas.um.es/red/article/view/410121

If you are interested in using LearningML for your research and need some help, want to give us some feedback or whatever you need about the project, you can
email us at `develop@learningml.es`.

# References

This document was adapted from the open-source contribution guidelines for [Facebook's Draft](https://github.com/facebook/draft-js/blob/a9316a723f9e918afde44dea68b5f9f39b7d9b00/CONTRIBUTING.md)

# Affero GPL License

LearningML, the easiest way to learn Machine Learning fundamentals

Copyright (C) 2020 Juan David Rodríguez García & KGBL-3

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
