import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ConfigService } from 'src/app/services/config.service';
import { User } from 'src/app/models/user.model';
import { concat } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-ml-signup',
  templateUrl: './ml-signup.component.html',
  styleUrls: ['./ml-signup.component.css'],
})
export class MlSignupComponent implements OnInit {

  isLinear = true;
  usernameFormGroup: FormGroup;
  emailFormGroup: FormGroup;
  birthdateFormGroup: FormGroup;
  genderFormGroup: FormGroup;
  @ViewChild('stepper', { static: true }) stepper: ElementRef;

  registrationData = {};

  helpingCards = {
    username: {
      title: "signup.helpingcard.username.title",
      text: "signup.helpingcard.username.text",
      image: ConfigService.settings.easyml.url + '/assets/images/Genio_pose_1-2.png'
    },
    email: {
      title: "signup.helpingcard.email.title",
      text: "signup.helpingcard.email.title",
      image: ConfigService.settings.easyml.url + '/assets/images/Genio_pose_2-2.png'
    },
    birthdate: {
      title: "signup.helpingcard.birthdate.title",
      text: 'signup.helpingcard.birthdate.text',
      image: ConfigService.settings.easyml.url + '/assets/images/Genio_pose_1-2.png'
    },
    gender: {
      title: "signup.helpingcard.gender.title",
      text: "signup.helpingcard.gender.text",
      image: ConfigService.settings.easyml.url + '/assets/images/Genio_pose_2-2.png',
    },

    end: {
      title: "signup.helpingcard.end.title",
      text: "signup.helpingcard.end.text",
      image: ConfigService.settings.easyml.url + '/assets/images/Genio_pose_3-2.png'
    },

  };

  helpingCard = {
    title: null,
    text: null,
    image: null
  };

  months = [
    { value: '1', viewValue: 'signup.january' },
    { value: '2', viewValue: 'signup.february' },
    { value: '3', viewValue: 'signup.march' },
    { value: '4', viewValue: 'signup.april' },
    { value: '5', viewValue: 'signup.may' },
    { value: '6', viewValue: 'signup.june' },
    { value: '7', viewValue: 'signup.july' },
    { value: '8', viewValue: 'signup.august' },
    { value: '9', viewValue: 'signup.september' },
    { value: '10', viewValue: 'signup.october' },
    { value: '11', viewValue: 'signup.november' },
    { value: '12', viewValue: 'signup.december' },

  ];

  years = [];

  genders = [
    { value: 'M', viewValue: 'signup.boy' },
    { value: 'F', viewValue: 'signup.girl' },
    { value: 'N', viewValue: 'signup.nowanttosay' },
  ];

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  url = ConfigService.settings.api.url_base +
    ConfigService.settings.api.path;

  url_signup = this.url + 'signup';

  constructor(private formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private authService: AuthenticationService,
    private translate: TranslateService) {

    if (localStorage.getItem("language") == null) {
      this.translate.setDefaultLang("es");
    } else {
      this.translate.setDefaultLang(localStorage.getItem("language"))
    }
    
    this.helpingCard = this.helpingCards.username;
    for (let i = new Date().getFullYear(); i >= 1900; i--) {
      this.years.push(i);
    }
  }

  validateUsernameNotTaken(control: AbstractControl) {
    return this.authService.checkUsernameNotTaken(control.value);
  }

  ngOnInit() {
    this.usernameFormGroup = this.formBuilder.group({
      usernameCtrl: ['', Validators.required, this.validateUsernameNotTaken.bind(this)],
      passwordCtrl: ['', Validators.required],
    });
    this.emailFormGroup = this.formBuilder.group({
      emailCtrl: ['', Validators.required]
    });
    this.birthdateFormGroup = this.formBuilder.group({
      monthCtrl: ['', Validators.required],
      yearCtrl: ['', Validators.required]
    });
    this.genderFormGroup = this.formBuilder.group({
      genderCtrl: ['', Validators.required]
    });
  }

  sendUserData() {
    // console.log(this.usernameFormGroup);
    if (this.usernameFormGroup.controls.usernameCtrl.errors && this.usernameFormGroup.controls.usernameCtrl.errors.usernameTaken) return;
    this.helpingCard = this.helpingCards.email;
    this.registrationData["username"] = this.usernameFormGroup.controls.usernameCtrl.value;
    this.registrationData["password"] = this.usernameFormGroup.controls.passwordCtrl.value;
  }

  sendEmailData() {
    this.helpingCard = this.helpingCards.birthdate;
    // console.log(this.emailFormGroup);
    if (this.emailFormGroup.status == "INVALID") return;
    this.registrationData["email"] = this.emailFormGroup.controls.emailCtrl.value;
  }

  sendBirthDateData() {
    this.helpingCard = this.helpingCards.gender;
    // console.log(this.birthdateFormGroup);
    this.registrationData['month'] = this.birthdateFormGroup.controls.monthCtrl.value;
    this.registrationData['year'] = this.birthdateFormGroup.controls.yearCtrl.value;
  }

  sendGenderData() {
    this.helpingCard = this.helpingCards.end;
    // console.log(this.genderFormGroup)
    this.registrationData['gender'] = this.genderFormGroup.controls.genderCtrl.value;
    this.onSubmit();
  }

  sendReset() {
    this.helpingCard = this.helpingCards.username;
  }

  nextStep(e) {
    switch (e.selectedIndex) {
      case 1:
        this.sendUserData();
        break;
      case 2:
        this.sendEmailData();
        break;
      case 3:
        this.sendBirthDateData();
        break;
      case 4:
        this.sendGenderData();
        break;
    }
  }

  startLML() {
    window.location.href = ConfigService.settings.easyml.url;
  }

  onSubmit() {
    // console.log("hola");
    // console.log(this.registrationData);

    const user = new User();
    user.username = this.registrationData['username'];
    user.password = this.registrationData['password'];


    const signup = this.httpClient.post(this.url_signup, this.registrationData, this.httpOptions);
    const login = this.authService.login(user);

    signup.subscribe(
      v => {
        login.subscribe(
          v => {
            this.helpingCard['end']['image'] = '/assets/images/Genio_pose_3-2.png';
          },
          e => {
            this.helpingCard['end']['image'] = '/assets/images/Genio_pose_4-2.png';
            this.helpingCard['end']['text'] = 'Ooohhh ha habido un error al crear la cuenta, intentalo más tarde.'
          }
        )
        setTimeout(() => {
          window.location.href = ConfigService.settings.easyml.url;
        }, 2000);
        
      },
      e => {
        this.helpingCard['end']['image'] = '/assets/images/Genio_pose_3-2.png';
        this.helpingCard['end']['text'] = 'Ooohhh ha habido un error al crear la cuenta, intentalo más tarde.';
      }
    );
  }

}
