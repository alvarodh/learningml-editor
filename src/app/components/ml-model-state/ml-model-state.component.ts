import { Component, OnInit } from '@angular/core';
import { LabeledDataManagerService } from 'src/app/services/labeled-data-manager.service';

@Component({
  selector: 'app-ml-model-state',
  templateUrl: './ml-model-state.component.html',
  styleUrls: ['./ml-model-state.component.css']
})
export class MlModelStateComponent implements OnInit {

  constructor(
    public labeledDataManager: LabeledDataManagerService,
    ) { }

  ngOnInit() {
  }

}
