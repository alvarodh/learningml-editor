import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { ClassifierService } from '../../services/classifier.service';
import { ILabeledText } from '../../interfaces/interfaces';
import { LabeledDataManagerService } from '../../services/labeled-data-manager.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ConfigService } from 'src/app/services/config.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-ml-model-toolbar',
  templateUrl: './ml-model-toolbar.component.html',
  styleUrls: ['./ml-model-toolbar.component.css']
})
export class MlModelToolbarComponent implements OnInit {

  name: string;
  labeledText: ILabeledText;
  @ViewChild("inputNameModel", { static: true }) inputNameModel: ElementRef;

  constructor(
    public labeledDataManager: LabeledDataManagerService,
    private router: Router,
    public classifierService: ClassifierService,
    private dialog: MatDialog,
    private translate: TranslateService,) {
  }

  ngOnInit() {

  }

  getState() {
    return this.classifierService.getState();
  }

  updateModelName(event) {
    // console.log(event);
    if (event.key == "Enter" || event.type == "blur") {

    }
  }

  openMyStuff() {
    this.router.navigate(['/projects']);
  }

  openSharedStuff() {
    this.router.navigate(['/sharedprojects']);
  }

  goHome() {
    this.router.navigate(['']);
  }

  tutorials() {
    let url = ConfigService.settings.web.tutorials;
    console.log(url);
    window.open(url, "_blank");
  }

  manual() {
    let url = ConfigService.settings.web.manual;
    console.log(url);
    window.open(url, "_blank");
  }

  selectLanguage(lang){
    localStorage.setItem("language", lang);
    this.translate.setDefaultLang(lang);
  }

  about() {
    const dialogRef = this.dialog.open(MlAboutDialogComponent, {
      width: '600px',
      data: ""
    });
  }

  learn() {
    let urlWeb = ConfigService.settings.api.url_base + '/aprende';
    console.log(urlWeb);
    window.open(urlWeb, "_blank");
  }
}


@Component({
  templateUrl: 'ml-about.html',
  styleUrls: ['ml-about.css']
})
export class MlAboutDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<MlAboutDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string) { }

  close(event) {
    // console.log(event);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}