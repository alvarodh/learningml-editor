import { Component, OnInit, Inject, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Label, TText, ExampleData } from 'src/app/interfaces/interfaces';
import { ClassifierService } from '../../services/classifier.service';
import { ScratchManagerService } from '../../services/scratch-manager.service';
import { LabeledDataManagerService } from '../../services/labeled-data-manager.service';
import { from } from 'rxjs';
import { MlWebCamComponent } from '../ml-web-cam/ml-web-cam.component';
import { TranslateService } from '@ngx-translate/core';


export type DialogData = string;

const IMAGE_SIZE = 227;
@Component({
  selector: 'app-ml-label-container',
  templateUrl: './ml-label-container.component.html',
  styleUrls: ['./ml-label-container.component.css']
})
export class MlLabelContainerComponent implements OnInit {

  panelOpenState = true;
  @ViewChild('fileImagesElement', { static: true }) fileElement: ElementRef;
  @ViewChild('webcam', { static: false }) webcam: MlWebCamComponent;
  @Input('items') items: ExampleData[];
  @Input('label') label: Label;
  @Output() onChildDeleted = new EventEmitter<Label>();

  constructor(
    public labeledDataManager: LabeledDataManagerService,
    private classifierService: ClassifierService,
    private scratchManager: ScratchManagerService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private translate: TranslateService) {

    this.items = [];

  }

  ngOnInit() {
  }

  addTerm() {

    let keys_to_be_translated = [
      'model.text_added'
    ];
    this.translate.get(keys_to_be_translated).subscribe((res: string) => {
      const dialogRef = this.dialog.open(MlLabelContainerDialogComponent, {
        width: '250px',
        data: ""
      });

      dialogRef.afterClosed().subscribe(result => {
        // console.log('The dialog was closed');
        if (result == "" || result == undefined) return;
        // This is the first text to be added in the label set
        this.items = this.items.concat([result]);
        let entry = { label: this.label, data: result };
        this.labeledDataManager.addEntry(entry);
        //this.classifierService.addEntry(entry);
        this.scratchManager.modelUpdated = false;
        this.snackBar.open(res["model.text_added"],
          '', {
          duration: 2000,
        });
      });

    });
  }

  addImages() {
    this.fileElement.nativeElement.click();
  }

  delete(text: string) {


    let keys_to_be_translated = [
      'model.text_deleted'
    ];
    this.translate.get(keys_to_be_translated).subscribe((res: string) => {
      const dialogRef = this.dialog.open(MlDeleteConfirmComponent, {
        width: '250px',
        data: text
      });

      dialogRef.afterClosed().subscribe(result => {
        // console.log('The dialog was closed');
        //  Borrado inmutable de elemento
        if (result == undefined) return;
        this.items = this.items.filter(o => o != result);
        let entry = { label: this.label, data: result };
        this.labeledDataManager.removeEntry(entry);
        //this.classifierService.removeEntry(entry);
        this.scratchManager.modelUpdated = false;
        this.snackBar.open(res['model.text_deleted'],
          '', {
          duration: 2000,
        });
      });
    })

  }

  deleteLabel() {
    let keys_to_be_translated = [
      'model.label_deleted'
    ];
    this.translate.get(keys_to_be_translated).subscribe((res: string) => {
      const dialogRef = this.dialog.open(MlDeleteConfirmComponent, {
        width: '250px',
        data: this.label
      });

      dialogRef.afterClosed().subscribe(result => {
        // console.log('The dialog was closed');
        this.labeledDataManager.removeLabel(result);
        this.onChildDeleted.emit(result);

        this.snackBar.open(res['model.label_deleted'] + ' `' + result + '`',
          this.label, {
          duration: 2000,
        });
      });
    })

  }

  initWebcam() {
    this.webcam.enableVideo();
  }

  takeSnapshot(b64Img) {
    //// console.log(b64Img);
    let image = new Image();
    image.src = b64Img;
    image.onload = () => {
      //// console.log(image);
      this.items = this.items.concat([image]);
      this.labeledDataManager.addEntry({ label: this.label, data: image });
    }
  }



  onLoaded(e) {
    let files = e.target.files;

    //// console.log(files);

    for (let file of files) {
      let fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = e => {
        let imageUrl = fileReader.result.toString();
        //// console.log(imageUrl);
        let image = new Image();
        image.src = imageUrl;
        image.onload = () => {
          //// console.log(image);
          this.items = this.items.concat(image);
          this.labeledDataManager.addEntry({ label: this.label, data: image });
        }

      }
    }
    //// console.log(this.labeledDataManager.labelsWithData);
  }
}

@Component({
  templateUrl: 'ml-label-container-dialog.html',
})
export class MlLabelContainerDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<MlLabelContainerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  templateUrl: 'ml-confirm-dialog.html',
})
export class MlDeleteConfirmComponent {

  constructor(
    public dialogRef: MatDialogRef<MlDeleteConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}



